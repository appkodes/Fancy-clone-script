# Fancy-clone-script

Fantacy is a heavily influenced e-commerce platform which involves in showcasing fashionary products. Fashion Business dreams come real through Fantacy which helps you to make a lot of money. 
Appkodes is introducing an exciting ecommerce product called Fantacy which is a fancy clone script. The fancy clone script allows building a site that provides flexible shopping. The trendy script allows the user to follow someone. Consumers may flatly browse through the product pictures, A Fancy clone is a multi-vendor clone script in the marketplace with buy and sell based shopping environment and is a place to explore trendy things to collect all of the cool things you want.

Version 4.0:

Separate Merchant portal for sellers to manage the products, orders and more details
New UI update to all the platforms.
Braintree payment to replace old paypal.
Mail chimp was used for newsletter option
Seller can add his product to Daily deal to offer the discounts
Multiple currency payment supportable
Share a product to Facebook and get discount
Product selfies from verified buyer’s
Improved notification options

Fantacy Multi-Vendor system is extremely easy to use for every Customer, Merchant, and Admin.
Buyers may now search much more easy with Barcode Search option and add collections to their wishlist.
Best offers and deal of the day can be listed here so that people will be more interested in purchasing products from daily deals.

Product Details

Each product page has a wide range of details about the product with Banner images, description and also the popular product.
Buyers may Like Share & Comment a product with any users who they think they might be interested.
Buyers may also upload a selfie with the received product using Product Selfie option which gives an authenticity for the seller and other users.

Demo: http://fantacyscript.com/